package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;




public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular(){
		int temperature = Fahrenheit.fromCelsius(5);
		assertTrue("Valid Temperature", temperature == 41);
	}
	
	@Test
	public void testFromCelsiusBoundaryException(){
		int temperature = Fahrenheit.fromCelsius(-6);
		assertFalse("Valid Temperature", temperature == 32);
	}
	
	@Test
	public void testFromCelsiusBoundaryIn(){
		int temperature = Fahrenheit.fromCelsius(0);
		assertTrue("Valid Temperature", temperature == 32);
	}
	
	@Test
	public void testFromCelsiusBoundaryOut(){
		int temperature = Fahrenheit.fromCelsius(-1);
		assertFalse("Valid Temperature", temperature == 32);
	}
	
	



}
