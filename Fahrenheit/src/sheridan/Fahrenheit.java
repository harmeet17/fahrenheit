package sheridan;

public class Fahrenheit {


	public static int fromCelsius(int celsius)
	{
		double f = (celsius * 9/5) + 32;
		int fahrenheit = (int) Math.round(f);
		return fahrenheit;
	}
}
